# LiXin_work

#### 介绍
立芯嵌入式学习项目

#### 软件架构
软件架构说明

采用CubeMx + cmake + arm-none-eabi-gcc + openocd开发调试

安装好运行环境后

最小单位是project文件夹，cubemx工程.ioc文件名与文件夹名绑定，所以统一将工程名为project
1. 新建工程可直接复制粘贴lesson_1文件夹
将.ioc文件以记事本打开可修改CubeMX最低版本号便于不同版本打开移植
2. 直接按照配置生成中间文件。
3. 将外部的freertos.c文件替换原本Core/freertos.c
4. 对照示例CMake添加以下几句，可以自动将src/drivers_ src/app_文件夹下的源文件加入工程
```cmake
aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR}/src/drivers_ DRIVER_SRC)
aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR}/src/app_ APP_SRC)

# Add sources to executable
target_sources(${CMAKE_PROJECT_NAME} PRIVATE
    # Add user sources here
    ${APP_SRC}
    ${DRIVER_SRC}
)

# Add include paths
target_include_directories(${CMAKE_PROJECT_NAME} PRIVATE
    # Add user defined include paths
    ${CMAKE_CURRENT_SOURCE_DIR}/src/app_
    ${CMAKE_CURRENT_SOURCE_DIR}/src/drivers_
)
```
5. cmake配置、编译、下载在.vscode/tasks.json中配置