#include "main.h"

#include "led_driver.h"

void task_main()
{
    LED_Init(&led1);
    while (1)
    {
        LED_Toggle(&led1);
        HAL_Delay(1000);
    }
}


#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
#include "stdio.h"
#include "usart.h"

osThreadId_t TaskA_Handle;  // 任务id
const osThreadAttr_t TaskA__attributes = {
  .name = "TaskA",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};                          // 任务属性
void TaskA_Func(void *argument);    // 任务函数

void freertos_init(void)
{
    // 创建任务
    TaskA_Handle = osThreadNew(TaskA_Func, NULL, &TaskA__attributes);
}

// 任务函数实体
void TaskA_Func(void *argument)
{
    static uint16_t i = 0;
    for(;;)
    {
        printf("%d: Hello World\n", i);
        osDelay(100);
        if ((++i) >= 20)
        {
            osThreadTerminate(TaskA_Handle); // 中止任务
        }
    }
}