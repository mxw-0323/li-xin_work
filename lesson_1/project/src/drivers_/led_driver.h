#ifndef __LED_H__
#define __LED_H__

#include "main.h"
#include "gpio.h"

typedef struct Led
{
    GPIO_TypeDef *port;
    uint16_t pin;
}led_t;

extern led_t led1, led2, led3, led4;

void LED_Init(led_t *led);
void LED_On(led_t *led);
void LED_Off(led_t *led);
void LED_Toggle(led_t *led);

#endif // !__LED_H__