#include "led_driver.h"

led_t led1 = {GPIOE, GPIO_PIN_3};
led_t led2 = {GPIOD, GPIO_PIN_7};
led_t led3 = {GPIOG, GPIO_PIN_3};
led_t led4 = {GPIOA, GPIO_PIN_5};

void LED_Init(led_t *led)
{
    if (led->port == GPIOA)
        __HAL_RCC_GPIOA_CLK_ENABLE();
    else if (led->port == GPIOD)
        __HAL_RCC_GPIOD_CLK_ENABLE();
    else if (led->port == GPIOE)
        __HAL_RCC_GPIOE_CLK_ENABLE();
    else if (led->port == GPIOG)
        __HAL_RCC_GPIOG_CLK_ENABLE();
    
    GPIO_InitTypeDef GPIO_InitStruct = {0};


    GPIO_InitStruct.Pin = led->pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(led->port, &GPIO_InitStruct);
}

void LED_On(led_t *led)
{
    HAL_GPIO_WritePin(led->port, led->pin, GPIO_PIN_SET);
}

void LED_Off(led_t *led)
{
    HAL_GPIO_WritePin(led->port, led->pin, GPIO_PIN_RESET);
}

void LED_Toggle(led_t *led)
{
    HAL_GPIO_TogglePin(led->port, led->pin);
}
